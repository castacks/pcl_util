/*
* Copyright (c) 2016 Carnegie Mellon University, Author <dimatura@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/**
 * Copyright (c) 2015 Carnegie Mellon University, Daniel Maturana <dimatura@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#include <iostream>
#include <pcl_util/pcl_util.hpp>

int main(int argc, char *argv[]) {
  float rgb = ca::pcl_util::int_to_pcl_rgb(49, 48, 49);
  std::cout << "rgb = " << rgb << "\n";

  return 0;
}
