# pcl_util

**Author: ** Daniel Maturana (dimatura@cmu.edu)

**Maintainer: ** Daniel Maturana (dimatura@cmu.edu)

Mish-mash of stuff useful for working with pcl. Some of them useful for clouds from the NEA M3 sensor.

- ** nea_pc_format.hpp **: Parse PointCloud2 binary blobs in the NEA format. PCL point type definition.
- ** nea_to_pcl.hpp **: Utility functions to convert NEA PointCloud2 blobs to PCL types.
- ** pcl_util.hpp **: Misc utility functions for point clouds.
- ** point_cloud2_iterator.h **: Adapted from ROS with bugfix.
- ** point_types.hpp **: Some convenience typedefs for PCL.
- ** transform_nea_pc.hpp **: Utility functions for rigid transforms of point clouds.


See `src/` and `test/` for some examples.





** TODO: **

- Update NEA stuff for newer data formats.
- Update tests.
- Documentation.
- pointcloud2 iterator could potentially be removed if bugfix has been committed upstream.

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
